﻿DROP DATABASE IF EXISTS desarrollo; 
CREATE DATABASE desarrollo;
USE desarrollo;

# Comienzo de creación de tabla alumnos
CREATE TABLE alumnos(
  codigo INT,
  nombre VARCHAR(100),
  correo VARCHAR(100),
  PRIMARY KEY (codigo),
  UNIQUE KEY (correo)
);
# fin creación tabla alumnos

# Comienzo creacion tabla exámenes
CREATE TABLE examenes (
  id int AUTO_INCREMENT,
  titulo varchar(100),
  nota float,
  fecha date, 
  codigoAlumno int,
  PRIMARY KEY (id) 
);
# fin creación tabla exámenes

# CREANDO LA FOREIGN KEY (clave ajena) DE EXAMENES
# Modificamos con alter table una tabla que ya exista
ALTER TABLE examenes ADD 
  CONSTRAINT FKExamenesAlumnos 
  FOREIGN KEY (codigoAlumno)
  REFERENCES alumnos (codigo);

#introducir datos en la tabla alumnos
INSERT INTO alumnos 
  (codigo, nombre, correo) VALUES
  ( 1, "Eva", "eva@alpe.es"),
  ( 2 , "Luis" , "luis@alpe.es");

#introducir datos en la tabla exámenes

INSERT INTO examenes 
  ( titulo, nota, fecha, codigoAlumno ) VALUES
    ("php", 9, "2023-05-01", 1 ),
    ("php", 8, "2023-04-20", 2 );  