DROP DATABASE desarrollo; # Si la base de datos ya existe, elimina la base de datos
CREATE DATABASE desarrollo; # Crea base de datos
USE desarrollo; -- Selecciona la base de datos para usarla


# Comienza la creación de la tabla alumnos
CREATE TABLE alumnos(
	codigo INT, 
	nombre VARCHAR(100),
	nota FLOAT,
	fechaNacimiento DATE,
	PRIMARY KEY (codigo)
);
# Fin de creación de la tabla alumnos

# Insertar registros en la tabla alumnos
INSERT INTO alumnos
 	(codigo, nombre, nota, fechaNacimiento) VALUES 
	 ( 1,	"Eva" , 8, "2001-12-14"	),
	 (2,"Luis" , 8, "1989-5-4");
# Fin de la inserción de registros


